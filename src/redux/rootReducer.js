import { combineReducers } from "redux";
import userListReducer from "../modules/userList/userList.reducer";
import userProfileReducer from "../modules/userProfile/userProfile.reducer";

const RootReducer = combineReducers({
    userListReducer,
    userProfileReducer
})

export default RootReducer
