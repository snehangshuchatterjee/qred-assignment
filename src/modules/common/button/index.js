import React from "react";
import { Button } from "react-bootstrap";

const ButtonComponent = (props) => {
    const {label, onClick} = props;

    return(
        <Button variant="primary" style={{width: '100%'}} onClick={onClick}>{label}</Button>
    );
}

export default ButtonComponent