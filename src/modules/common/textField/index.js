import React, {useState} from "react";
import { Form, Row } from "react-bootstrap";

const getCamelCasedText = (text) => {
    return text.charAt(0).toUpperCase() + text.slice(1, text.length)
}

const TextField = (props) => {
    const { className, label, placeholder, type, value, onChange } = props;
    const [isInvalid, setInvalid] = useState(false);
    let inputType = label.toLowerCase() === 'email' ? 'email' : 'text';

    const handleValueChange = (input) => {
        onChange(label.toLowerCase(), input.currentTarget.value)
    }

    const validateInput = (input) => {
        const currentValue = input.currentTarget.value;

        if (inputType === 'email') {
            if (currentValue.lastIndexOf('@') > 0 && currentValue.lastIndexOf('.') > currentValue.lastIndexOf('@')) {
                setInvalid(false);
            }
            else {
                setInvalid(true);
            }
        }
    }

    return(
        <Row>
            <Form>
                <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>{getCamelCasedText(label)}</Form.Label>
                    <Form.Control type={inputType} placeholder={placeholder} className={className} defaultValue={value} onChange={handleValueChange} onBlur={validateInput} isInvalid={isInvalid}/>
                </Form.Group>
                </Form>
        </Row>
    );
}

export default TextField;