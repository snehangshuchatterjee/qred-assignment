import React from "react";
import { Table } from "react-bootstrap";

const getTableHeadersArray = (tableDataItem) => {
    return Object.keys(tableDataItem);
}

const getTableHeaders = (tableHeadersArray) => {
    return(
        <thead>
            <tr>
            {tableHeadersArray.map((header, index) => (
                <th key={index}>{header}</th>
            ))}
            </tr>
        </thead>
    );
}

const getTableData = (tableHeadersArray, tableData) => {
    let result = [];

    result = tableData.map(tableDataItem => {
        return (<tr>
            {tableHeadersArray.map((header, index) => (
                header !== 'address' && header !== 'company' && <td key={index}>{tableDataItem[header]}</td>
            ))}
        </tr>)
    });

    console.log(`Data is: ${result}`);

    return(
        <tbody>
            {result}            
        </tbody>
    );
}

const TableComponent = (props) => {
    const {tableData} = props;
    const tableHeadersArray = getTableHeadersArray(tableData[0]);

    return(
        <Table responsive>
            {getTableHeaders(tableHeadersArray)}
            {getTableData(tableHeadersArray, tableData)}
        </Table>
    );
}

export default TableComponent;