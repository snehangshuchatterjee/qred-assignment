import {
  FETCH_USER_DETAILS,
  FETCH_USER_DETAILS_FAIL,
  FETCH_USER_DETAILS_SUCCESS,
  UPDATE_USER_DETAILS,
  UPDATE_USER_DETAILS_FAIL,
  UPDATE_USER_DETAILS_SUCCESS
} from "./userProfile.actions";

const initialState = {
    loading: false,
  userDetails: {},
    status: 0,
    error: ''
};

export default function userProfileReducer(state = initialState, action) {
    switch (action.type) {
      case FETCH_USER_DETAILS: {
        return {
            ...state,
            loading: true,
            error: false,
          }    
      }
      case FETCH_USER_DETAILS_FAIL: {
        return {
            ...state,
            loading: false,
            error: true,
        }    
      }
      case FETCH_USER_DETAILS_SUCCESS: {
        return {
            ...state,
            loading: false,
            userDetails: action.payload.data,
          }    
      }
      case UPDATE_USER_DETAILS: {
        return {
            ...state,
            loading: true,
            error: false,
          }    
      }
      case UPDATE_USER_DETAILS_FAIL: {
        return {
            ...state,
            loading: false,
            error: true,
        }    
      }
      case UPDATE_USER_DETAILS_SUCCESS: {
        return {
            ...state,
            loading: false,
            status: action.payload.status,
          }    
      }
      default: {
        return initialState
      }
    }
}  