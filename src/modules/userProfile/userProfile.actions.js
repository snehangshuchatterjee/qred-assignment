export const FETCH_USER_DETAILS = "FETCH_USER_DETAILS";
export const FETCH_USER_DETAILS_FAIL = "FETCH_USER_DETAILS_FAIL";
export const FETCH_USER_DETAILS_SUCCESS = "FETCH_USER_DETAILS_SUCCESS";
export const UPDATE_USER_DETAILS = "UPDATE_USER_DETAILS";
export const UPDATE_USER_DETAILS_FAIL = "UPDATE_USER_DETAILS_FAIL";
export const UPDATE_USER_DETAILS_SUCCESS = "UPDATE_USER_DETAILS_SUCCESS";

export const fetchUserDetails = (id) => ({
    type: FETCH_USER_DETAILS,
    payload: {
      request: {
        url: `/users/${id}`,
      },
    },
  }
)

export const updateUserDetails = (id, updatedDetails) => ({
    type: UPDATE_USER_DETAILS,
    payload: {
      request: {
        url: `/users/${id}`,
        method: 'PUT',
        data: updatedDetails
      },
    },
  }
)
