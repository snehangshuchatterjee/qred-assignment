import { Alert, Col, Row } from "react-bootstrap";
import React, { useEffect } from "react";
import { connect } from "react-redux";
import { useParams } from "react-router-dom";

import { fetchUserDetails, updateUserDetails } from "./userProfile.actions";
import "./userProfile.scss";
import TextField from "../common/textField";
import Header from "../header";
import Footer from "../footer";
import ButtonComponent from "../common/button";

const mapStateToProps = (state) => {
  return {
    loading: state.userProfileReducer.loading,
    userDetails: state.userProfileReducer.userDetails,
    status: state.userProfileReducer.status,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    fetchUserDetails: (id) => dispatch(fetchUserDetails(id)),
    updateUserDetails: (id, updatedUser) =>
      dispatch(updateUserDetails(id, updatedUser)),
  };
};

const getUserHeadings = (userData) => {
  let result = [];

  if (userData) result = Object.keys(userData);

  return result;
};

const getUserAddressHeadings = (userAddressData) => {
  return Object.keys(userAddressData);
};

const UserProfile = (props) => {
  const { id } = useParams();
  const { loading, userDetails, status, fetchUserDetails, updateUserDetails } =
    props;

  const handleUserDataChange = (key, value) => {
    userDetails[key] = value;
  };

  const modifyUserDetails = () => {
    updateUserDetails(id, userDetails);
  };

  useEffect(() => {
    fetchUserDetails(id);
  }, [id]);

  const userDetailsHeadings = getUserHeadings(userDetails);
  const userAddressHeadings =
    userDetails?.address && getUserAddressHeadings(userDetails.address);
  const userAddress = userDetails.address;

  return (
    <>
      <Header />
      <div className="container">
        <Row className="heading">
          <Col>Edit your Profile</Col>
        </Row>
        <Row>
          <Col></Col>
          <Col className="user-profile" md={10} xs={12}>
            <Row>
              <Col></Col>
              <Col md={10} xs={12}>
                {status === 200 && (
                  <Alert variant={"success"}>
                    Data Updated Successfully....!!
                  </Alert>
                )}
                {status !== 0 && status !== 200 && (
                  <Alert variant={"danger"}>
                    There was some problem saving the data.
                  </Alert>
                )}
                {!loading &&
                  userDetailsHeadings.length > 0 &&
                  userDetailsHeadings.map(
                    (heading, index) =>
                      !["id", "address", "company"].includes(heading) && (
                        <TextField
                          key={index}
                          label={heading}
                          className="form-control-button"
                          value={userDetails[heading]}
                          onChange={handleUserDataChange}
                        />
                      )
                  )}
                {userDetails?.company && (
                  <TextField
                    label={"company"}
                    className="form-control-button"
                    value={userDetails["company"].name}
                    onChange={handleUserDataChange}
                  />
                )}
                {!loading &&
                  userAddressHeadings?.length > 0 &&
                  userAddressHeadings.map(
                    (heading, index) =>
                      heading !== "geo" && (
                        <TextField
                          key={index}
                          label={heading}
                          className="form-control-button"
                          value={userAddress[heading]}
                          onChange={handleUserDataChange}
                        />
                      )
                  )}
                <Row id="edit-button">
                  <Col></Col>
                  <Col md={6} xs={12}>
                    <ButtonComponent
                      label="Save Changes"
                      onClick={modifyUserDetails}
                    />
                  </Col>
                  <Col></Col>
                </Row>
              </Col>
              <Col></Col>
            </Row>
          </Col>
          <Col></Col>
        </Row>
      </div>
      <Footer />
    </>
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(UserProfile);
