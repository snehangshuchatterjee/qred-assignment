import React from 'react';
import { Container, Nav, Navbar} from 'react-bootstrap';
import { HEADER_LOGO_URL } from '../../constants';

import './header.scss';

const Header = (props) => {
    return(
        <Navbar className="navbar-header" expand="lg">
            <Container fluid>
                <Navbar.Brand href="#">
                    <img
                        src={HEADER_LOGO_URL}
                        className="header-logo-image"
                        alt="React Bootstrap logo"
                    />
                </Navbar.Brand>
                <Navbar.Toggle aria-controls="navbarScroll" />
                <Navbar.Collapse id="navbarScroll">
                <Nav
                    className="me-auto my-2 my-lg-0"
                    style={{ maxHeight: '100px' }}
                    navbarScroll
                >
                    <Nav.Link href="/">Home</Nav.Link>
                    <Nav.Link href="/users">Users</Nav.Link>
                </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    );
}

export default Header;