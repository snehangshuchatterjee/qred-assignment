import React, {useEffect} from "react";
import { connect } from "react-redux";

import './userList.scss';
import UserItem from "./userItem";
import { fetchListOfUsers } from "./userList.actions";
import Footer from "../footer";
import Header from "../header";

const mapStateToProps = (state) => {
    return({
        users: state.userListReducer.users
    })
}
const mapDispatchToProps = (dispatch) => {
    return({
        fetchListOfUsers: () => dispatch(fetchListOfUsers())
    })
}

const UserList = (props) => {
    const {fetchListOfUsers, users} = props;

    useEffect(()=>{
        fetchListOfUsers();
    }, []);

    return(
        <>
            <Header />
            <div className="container">
                <div className="user-list">
                    {users.length > 0 && users.map((user, index) => <UserItem userDetails={user} key={ index }/>)}
                </div>
            </div>
            <Footer />
        </>
    );
}

export default connect(mapStateToProps, mapDispatchToProps)(UserList);