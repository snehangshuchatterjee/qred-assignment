export const FETCH_USER_LIST = "FETCH_USER_LIST";
export const FETCH_USER_LIST_FAIL = "FETCH_USER_LIST_FAIL";
export const FETCH_USER_LIST_SUCCESS = "FETCH_USER_LIST_SUCCESS";

export const fetchListOfUsers = () => ({
    type: FETCH_USER_LIST,
    payload: {
      request: {
        url: `/users`,
      },
    },
  })
  