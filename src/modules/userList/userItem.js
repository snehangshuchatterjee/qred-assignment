import React from "react";
import { Card } from "react-bootstrap";

const UserItem = (props) => {
    const {userDetails} = props;
    const {email, name, username, website, id} = userDetails;

    return(
        <Card style={{ width: '18rem' }}>
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle className="mb-2 text-muted">@{username}</Card.Subtitle>
                <Card.Text>
                <br />
                Email: {email} <br />
                Website: {website}
                </Card.Text>
                <Card.Link href={`users/${id}`}>Edit</Card.Link>
            </Card.Body>
        </Card>
    );
}

export default UserItem