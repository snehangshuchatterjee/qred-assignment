import { FETCH_USER_LIST, FETCH_USER_LIST_FAIL, FETCH_USER_LIST_SUCCESS } from "./userList.actions";

const initialState = {
    loading: false,
    users: [],
    error: ''
};

export default function userListReducer(state = initialState, action) {
    switch (action.type) {
      case FETCH_USER_LIST: {
        return {
            ...state,
            loading: true,
            error: false,
          }    
      }
      case FETCH_USER_LIST_FAIL: {
        return {
            ...state,
            loading: false,
            error: true,
        }    
      }
      case FETCH_USER_LIST_SUCCESS: {
        return {
            ...state,
            loading: false,
            users: action.payload.data,
          }    
      }
      default: {
        return initialState
      }
    }
}  