import React from 'react';
import { Container, Navbar} from 'react-bootstrap';

import './footer.scss';

const Footer = () => {
    return (
        <Navbar className="navbar-footer" expand="lg" fixed='bottom'>
            <Container fluid>
            &copy; <em id="date"></em>Snehangshu Chatterjee for Qred
            </Container>
        </Navbar>
    );
}

export default Footer;