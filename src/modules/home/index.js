import React from "react";
import { Col, Row } from "react-bootstrap";
import Footer from "../footer";
import Header from "../header";
import './home.scss'

const Home = () => {
    return(
        <>
            <Header />
            <div className="container">
                <Row>
                    <Col></Col>
                    <Col className="home" md={10} xs={12}>
                        <p>This is the assignment for the position of React Developer. This application fetches the list of users, and gives you the option to edit the user's data.</p>

                        <p>I have tried to replicate the styling used in the Qred website. For Styling, I have used Bootstrap along with the library React-Bootstrap. </p>

                        <p>In order to use the application, click on the Users link, which will display the list of users. Each user item will have an option to edit the user's details. On clicking the 'Edit' link, the user details page will open, which will give you the option to modify the user's details and save it. On saving, an alert will be displayed according to the result of the save operation.</p>

                        <p>Currently, only the email field has validations. If an entry is invalid, there will be a red border around the email field. </p>
                    </Col>
                    <Col></Col>
                </Row>
            </div>
            <Footer />
        </>
    );
}

export default Home;