/* eslint-disable import/no-extraneous-dependencies */
import React from "react"
import { Router, Switch, Route } from "react-router-dom"
import { createBrowserHistory } from "history"

import Home from "../modules/home"
import UserList from "../modules/userList"
import UserProfile from "../modules/userProfile"

const history = createBrowserHistory()

function Routes() {
  return (
    <Router history={history}>
      <Switch>
        <Route path="/" exact component={Home} />
        <Route path="/users" exact component={UserList} />
        <Route path="/users/:id" exact component={UserProfile} />
      </Switch>
    </Router>
  )
}

export default Routes
